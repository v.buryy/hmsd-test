#!/usr/bin/env sh
set -e
set -v

docker info
docker-compose version

docker-compose -f docker-compose.yml ps

docker-compose -f docker-compose.yml stop
docker-compose -f docker-compose.yml rm -f
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml up -d