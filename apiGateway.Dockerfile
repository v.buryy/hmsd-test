FROM mcr.microsoft.com/dotnet/core/sdk:3.1
WORKDIR /app

COPY . .

RUN dotnet restore
RUN dotnet publish ApiGateway -c Release -o Publish

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app

COPY --from=0 /app/Publish/ .

ENTRYPOINT ["dotnet", "ApiGateway.dll"]