﻿using Microsoft.AspNetCore.Http;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using EncryptionApi.Models;

namespace EncryptionApi.Filters
{
    public static class ModelStateValidator
    {
        public static ValidationResponse Result(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = StatusCodes.Status422UnprocessableEntity;

            var messages = new List<string>();

            if (context.ModelState.TryGetValue(string.Empty, out var modelStateEntry))
            {
                messages.AddRange(modelStateEntry.Errors.Select(er => !string.IsNullOrEmpty(er.ErrorMessage)
                                ? er.ErrorMessage
                                : er.Exception?.Message).ToList());
            }

            return new ValidationResponse(messages);
        }
    }
}
