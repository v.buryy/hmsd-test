using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using EncryptionApi.Middlewares;
using EncryptionApi.Filters;
using EncryptionApi.Interfaces;
using EncryptionApi.Services;

namespace EncryptionApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.InvalidModelStateResponseFactory = context =>
                    {
                        return new UnprocessableEntityObjectResult(ModelStateValidator.Result(context));
                    };
                })
                .AddNewtonsoftJson();
            services.AddResponseCompression();

            // register healthchecks
            services.AddHealthChecks();

            // register services
            services.AddTransient<IEncryptionService, EncryptionService>();
            services.AddTransient<IKeyStorageService, KeyStorageService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionMiddleware>();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapControllers();
            });
        }
    }
}
