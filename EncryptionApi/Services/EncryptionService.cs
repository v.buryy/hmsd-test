﻿using EncryptionApi.Exceptions;
using EncryptionApi.Helpers;
using EncryptionApi.Interfaces;

namespace EncryptionApi.Services
{
    public class EncryptionService : IEncryptionService
    {
        private readonly IKeyStorageService _keyStorageService;

        public EncryptionService(IKeyStorageService keyStorageService)
        {
            _keyStorageService = keyStorageService;
        }

        public string Decrypt(string input)
        {
            return TryDecrypt(input);
        }

        public string Encrypt(string input)
        {
            var key = _keyStorageService.GetActiveKey();

            if (string.IsNullOrEmpty(key))
            {
                throw new ValidationException("Service can't process you request. Try again later.");
            }

            return EncryptDecryptHelper.Encrypt(input, key);
        }

        public void Rotate(string newKey)
        {
            _keyStorageService.AddKey(newKey);
        }

        private string TryDecrypt(string input)
        {
            foreach (var key in _keyStorageService.GetKeys())
            {
                try
                {
                    return EncryptDecryptHelper.Decrypt(input, key);
                }
                catch
                {
                    // log
                }
            }

            throw new ValidationException("Input string is not encrypted or decryption key already expired.");
        }
    }
}
