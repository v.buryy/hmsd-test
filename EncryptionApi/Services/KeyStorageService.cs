﻿using EncryptionApi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace EncryptionApi.Services
{
    /// <summary>
    /// Basic in-memory storage.
    /// For scaling should be used another storage implementation (db, files etc.)
    /// </summary>
    public class KeyStorageService : IKeyStorageService
    {
        private static SemaphoreSlim _mutex = new SemaphoreSlim(1);

        // Stores last 5 keys
        private static readonly Queue<string> _privateKeys = new Queue<string>();

        private static readonly int MaxKeys;

        static KeyStorageService()
        {
            if (int.TryParse(Environment.GetEnvironmentVariable("MAX_PRIVATE_KEYS_COUNT"), out int maxKeys) && maxKeys > 0)
            {
                MaxKeys = maxKeys;
            }
            else
            {
                MaxKeys = 5;
            }
        }

        public string GetActiveKey()
        {
            return _privateKeys.LastOrDefault();
        }

        public void AddKey(string newKey)
        {
            _mutex.Wait();

            try
            {

                if (_privateKeys.Count >= MaxKeys)
                {
                    _privateKeys.Dequeue();
                }

                _privateKeys.Enqueue(newKey);
            }
            catch
            {
                // Just rethrow exception.
                throw;
            }
            finally
            {
                // Always release mutex.
                _mutex.Release();
            }
        }

        public List<string> GetKeys()
        {
            return _privateKeys.ToList();
        }
    }
}
