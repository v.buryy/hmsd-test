﻿using System;
using System.Collections.Generic;

namespace EncryptionApi.Exceptions
{
    [Serializable]
    public class ValidationException : Exception
    {
        public List<string> Messages { get; }

        public ValidationException() : base()
        {
            Messages = new List<string>();
        }

        public ValidationException(string message) : base(message)
        {
            Messages = new List<string>
            {
                message
            };
        }

        public ValidationException(IEnumerable<string> messages) : base()
        {
            Messages = new List<string>();

            Messages.AddRange(messages);
        }
    }
}
