﻿using EncryptionApi.Interfaces;
using EncryptionApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace EncryptionApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EncryptionController : ControllerBase
    {
        private readonly IEncryptionService _encryptionService;

        public EncryptionController(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        [HttpPost("encrypt")]
        public IActionResult Encrypt(EncryptInputModel model)
        {
            var encryptionResult = _encryptionService.Encrypt(model.Input);

            return Ok(new OutputResponseModel(encryptionResult));
        }

        [HttpPost("decrypt")]
        public IActionResult Decrypt(DecryptInputModel model)
        {
            var decryptionResult = _encryptionService.Decrypt(model.Input);

            return Ok(new OutputResponseModel(decryptionResult));
        }

        [HttpPost("rotate")]
        public IActionResult Rotate(RotateKeyInputModel model)
        {
            _encryptionService.Rotate(model.Key);

            return Ok();
        }
    }
}
