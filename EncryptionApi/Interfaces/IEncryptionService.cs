﻿namespace EncryptionApi.Interfaces
{
    public interface IEncryptionService
    {
        string Encrypt(string input);

        string Decrypt(string input);

        void Rotate(string newKey);
    }
}
