﻿using System.Collections.Generic;

namespace EncryptionApi.Interfaces
{
    /// <summary>
    /// Inteface for key storage service.
    /// </summary>
    public interface IKeyStorageService
    {
        string GetActiveKey();

        void AddKey(string newKey);

        List<string> GetKeys();
    }
}
