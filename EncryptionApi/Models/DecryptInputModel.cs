﻿using System.ComponentModel.DataAnnotations;

namespace EncryptionApi.Models
{
    public class DecryptInputModel
    {
        [Required]
        public string Input { get; set; }
    }
}
