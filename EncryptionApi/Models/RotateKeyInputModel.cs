﻿using System.ComponentModel.DataAnnotations;

namespace EncryptionApi.Models
{
    public class RotateKeyInputModel
    {
        [Required]
        public string Key { get; set; }
    }
}
