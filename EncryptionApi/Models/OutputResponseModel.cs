﻿namespace EncryptionApi.Models
{
    public class OutputResponseModel
    {
        public OutputResponseModel(string output)
        {
            Output = output;
        }

        public string Output { get; set; }
    }
}
