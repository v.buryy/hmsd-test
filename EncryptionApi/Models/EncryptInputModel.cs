﻿using System.ComponentModel.DataAnnotations;

namespace EncryptionApi.Models
{
    public class EncryptInputModel
    {
        [Required]
        public string Input { get; set; }
    }
}
