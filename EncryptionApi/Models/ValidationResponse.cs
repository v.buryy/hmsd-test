﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EncryptionApi.Models
{
    public class ValidationResponse
    {
        public ValidationResponse()
        {
            ValidationMessages = new List<string>();
        }

        public ValidationResponse(List<string> messages)
        {
            ValidationMessages = messages;
        }

        [JsonProperty("errors")]
        public List<string> ValidationMessages { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
