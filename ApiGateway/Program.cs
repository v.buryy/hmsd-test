using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;

namespace ApiGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(options =>
                    {
                        options.Listen(IPAddress.Any, 5000);
                    });

                    webBuilder.UseStartup<Startup>();

                    webBuilder.ConfigureAppConfiguration((hostingContext, ic) =>
                    {
                        if (hostingContext.HostingEnvironment.IsDevelopment())
                        {
                            ic.AddOcelot("ocelot-dev", hostingContext.HostingEnvironment);
                        }
                        else
                        {
                            ic.AddOcelot("ocelot", hostingContext.HostingEnvironment);
                        }
                    });
                });
    }
}
