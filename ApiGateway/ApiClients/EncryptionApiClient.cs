﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiGateway.ApiClients
{
    public class EncryptionApiClient
    {
        protected readonly Uri _baseUrl;

        public EncryptionApiClient(string baseUrl)
        {
            _baseUrl = new Uri(baseUrl);
        }

        public async Task<HttpResponseMessage> PostAsync(string url, object data)
        {
            using (var apiClient = CreateClient())
            {
                var combineUrl = new Uri(_baseUrl, url);
                var body = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                return await apiClient.PostAsync(combineUrl, body);
            }
        }

        protected HttpClient CreateClient()
        {
            var apiClient = new HttpClient();

            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return apiClient;
        }
    }
}
