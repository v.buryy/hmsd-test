﻿using System;
using System.Security.Cryptography;

namespace ApiGateway.Helpers
{
    public static class RandomKeyGenerator
    {
        public static string GenerateRandomKey()
        {
            var rnd = RandomNumberGenerator.Create();
            var keyData = new byte[16];
            rnd.GetBytes(keyData);

            return Convert.ToBase64String(keyData);
        }
    }
}
