﻿using ApiGateway.ApiClients;
using ApiGateway.Helpers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ApiGateway.BackgroundServices
{
    public class KeyRotateBackgroundService : BackgroundService
    {
        private readonly EncryptionApiClient _encryptionApiClient;
        private static readonly int Delay;
        private const int ONE_SECOND = 1000;

        static KeyRotateBackgroundService()
        {
            if (int.TryParse(Environment.GetEnvironmentVariable("ROTATE_DELAY_SECONDS"), out int delaySeconds))
            {
                Delay = delaySeconds * ONE_SECOND;
            }
            else
            {
                // 5 min by default
                Delay = 5 * 60 * ONE_SECOND;
            }
        }

        public KeyRotateBackgroundService(EncryptionApiClient encryptionApiClient)
        {
            _encryptionApiClient = encryptionApiClient;
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (true)
            {
                if (stoppingToken.IsCancellationRequested)
                {
                    break;
                }

                var key = RandomKeyGenerator.GenerateRandomKey();

                // call api to rotate key
                var response = await _encryptionApiClient.PostAsync("api/encryption/rotate", new { Key = key });

                if (!response.IsSuccessStatusCode)
                {
                    // log, notify someone etc.
                }

                await Task.Delay(Delay, stoppingToken); // delay
            }
        }
    }
}
