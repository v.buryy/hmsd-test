#!/usr/bin/env sh
set -e
set -v

docker info
docker-compose version

docker-compose -f docker-compose.yml ps

docker-compose -f docker-compose.yml stop